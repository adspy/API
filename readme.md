<!-- ~/gh-md-toc readme.md -->

   * [Prerequisites](#prerequisites)
   * [Curl](#curl)
      * [Obtain OAuth token](#obtain-oauth-token)
      * [Make POST request to Ad endpoint](#make-post-request-to-ad-endpoint)
   * [Request Parameters](#request-parameters)
      * [siteType](#sitetype)
      * [gender](#gender)
      * [ages](#ages)
      * [dailyLikes](#dailylikes)
      * [totalLikes](#totallikes)
      * [mediaType](#mediatype)
      * [createdBetween](#createdbetween)
      * [seenBetween](#seenbetween)
      * [affNetwork](#affnetwork)
      * [affId](#affid)
      * [offerId](#offerid)
      * [username](#username)
      * [userId](#userId)
      * [lang](#lang)
      * [tech](#tech)
      * [buttons](#buttons)
      * [countries](#countries)
      * [orderBy](#orderby)
      * [page](#page)
      * [searches](#searches)
      * [details](#details)



# Prerequisites 
To use AdSpy API you need to subscribe to monthly API subscription additionally to your normal AdSpy subscription.

Note, that all parameters are case sensitive. 



# Curl
## Obtain OAuth token

```
curl -d "username=youremail@gmail.com&password=xxxxxx&grant_type=password" -X POST https://api.adspy.com/api/token
```
 


The response will be similar to

```
"access_token":"xxxx...xxx","token_type":"bearer","expires_in":863913599,"userName":"youremail@gmail.com","emailConfirmed":"True","subscriptionValid":"True",".issued":"Tue, 22 May 2018 08:50:18 GMT",".expires":"Fri, 06 Oct 2045 08:50:18 GMT"}
```

## Make POST request to Ad endpoint

```
curl -d "{seenBetween:['08-Apr-2018','09-Apr-2018'],orderBy:'total_loves'}" -X POST https://api.adspy.com/api/ad -H "Content-Type: application/json"  -H "authorization:Bearer your_token"
```

# Request Parameters

## siteType
`facebook` or `instagram`

Example: `{siteType:'instagram',...}`


## gender
`male` or `female`

Example: `{gender:'female',...}`

## ages
An array of one or two numbers representing min and max age. Values can be from 18 to 64. Second value should be bigger than the first one. If only one number is specified, this is considered min age.

Example: `{ages:[25],...}` or `{ages:[25,50],...}`

## dailyLikes
An array of one or two numbers representing min and max average daily likes. Values can be from 0 to 1000. Second value should be bigger than the first one. If only one number is specified, this is considered min average daily likes.

Example: `{dailyLikes:[100],...}` or `{dailyLikes:[100,500],...}`

## totalLikes
An array of one or two numbers representing min and max total likes. Values can be from 0 to 100000. Second value should be bigger than the first one. If only one number is specified, this is considered min total likes.

Example: `{totalLikes:[1000],...}` or `{totalLikes:[1000,50000],...}`

## mediaType
`video` or `photo`

Example: `{mediaType:'video',...}`

## createdBetween
An array of dates in `2-Apr-2018` format.

Example: `{createdBetween:['08-Apr-2018','09-Apr-2018'],...}`

## seenBetween
An array of dates in `2-Apr-2018` format.

Example: `{seenBetween:['08-Apr-2018','09-Apr-2018'],...}`

## affNetwork
Number indicating Affiliate Network ID. 
Full list can be obtained at https://api.adspy.com/api/AffNetwork endpoint

Example: `{affNetwork:822,...}`

## affId
String indicating affiliate ID. Exact match.

Example: `{affId:'123',...}`

## offerId
String indicating offer ID. Exact match.

Example: `{offerId:'123',...}`

## username
String indicating advertiser username. Exact match.

Example: `{username:'ufc',...}`

## userId
Number indicating advertiser user ID. Exact match. 

Example: `{userId: 46299886275,...}`

## lang
Three  letter language code of the ad. Codes are here https://github.com/wooorm/franc/tree/main/packages/franc

Example: `{lang: 'eng',...}`

## tech
An array of numbers of technology IDs. 

Full list can be obtained at https://api.adspy.com/api/tech endpoint

Example: `{tech:[350,390],...}`

## buttons
An array of strings with Button names. 

Example: `{buttons:['Shop Now','Sign Up'],...}`


## countries
An array of two letter country codes.

Example: `{countries:['US','UK'],...}`

## orderBy
String indicating ads display order. Possible values: `created_on_asc`,`total_likes`,`total_loves`,`total_hahas`,`total_wows`,`total_sads`,`total_angrys`,`total_shares`. Default order is by created on descending

Example: `{orderBy:total_loves,...}`

## page 
Page number. Every page contains 10 ads.

Example: `{page:10,...}`

## searches
Array of objects 

Examples:

Search some text: `{searches:[{type:'texts',value: 'christmas offer',locked: false}],....}`

Search ads by an advertiser: `{searches:[{type:'advertisers',value: 'shanghai9',locked: false}],....}`

Search ads containing a specific string in the URL: `{searches:[{type:'urls',value: 'nike.com',locked: false}],....}`

Search ads containing a specific string in the URL, found in landing pages: `{searches:[{type:'lp_urls',value: 'nike.com',locked: false}],....}`

Search ads with comments containing specific text: `{searches:[{type:'comments',value: 'awesome',locked: false}],....}`

Search ads with landing pages containing specific text: `{searches:[{type:'page_text',value: 'discount',locked: false}],....}`

Search ads containing specific text, published by a specific advertiser and having a string in ad URL    `{searches:[{type:'urls',value: 'nike.com',locked: false},{type:'advertisers',value: 'football',locked: false},{type:'texts',value: 'disponible',locked: false}]....}`

The `locked` property is used to change from `OR` to `AND` operator when multiple search objects of the same type are passed.

## details
String indicating whethere to bring Pages and Offers objects 

Example: `{details:true,...}`
